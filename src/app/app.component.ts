import { Component } from '@angular/core';
import { AppHttpService } from '../app/app-http.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'test';
  clientData: any;
  closeResult: string;
  clientDetail: Object;
  clientDetailData = [];
  searchText: any;
  registerForm: FormGroup;
  sumbit = false;
  updateClient: any;
  operationName: string;
  constructor(private apiHttp: AppHttpService, private formBuilder: FormBuilder, private modalService: NgbModal) {

  }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      age: ['', [Validators.required, Validators.maxLength(3), Validators.pattern("^[0-9]*$")]],
      salary: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
    });
    this.getClientDetails();
  }
  get f() { return this.registerForm.controls; }
  //data
  getClientDetails() {
    this.apiHttp.get().subscribe((data) => {
      console.log(data);
      this.clientData = data;
      this.clientDetailData.push(...this.clientData.data);
    });
  }
  onSubmit() {
    this.sumbit = true;
    if (this.registerForm.invalid) {
      return;
    }
    if (this.operationName === 'Add') {
      this.apiHttp.post(this.registerForm.value).subscribe((data) => {
        console.log(data);
        let addedClient = { id: 0, employee_name: "", employee_age: 0, employee_salary: 0 };
        addedClient.id = data.data.id;
        addedClient.employee_name = data.data.name;
        addedClient.employee_age = data.data.age;
        addedClient.employee_salary = data.data.salary;
        this.clientDetailData.push(addedClient);
      });
      // this.clientData.push(JSON.stringify(this.registerForm.value));
    }
    this.clientDetailData.forEach((client: any) => {
      if (client.id == this.updateClient.id) {
        client.employee_name = this.updateClient.employee_name;
        client.employee_age = this.updateClient.employee_age;
        client.employee_salary = this.updateClient.employee_salary;
      }

    });
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
  }
  //open modal data
  open(content, id) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.clientDetailData.forEach((client: any) => {
      if (client.id == id) {
        this.clientDetail = client;
      }
    });
  }
  open2(content, id, operation) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.updateClient = {};
    if (operation === "Add") {
      this.operationName = 'Add';
    }
    else {
      this.operationName = 'Update';
      this.clientDetailData.forEach((client: any) => {
        if (client.id == id) {
          this.updateClient.id = client.id;
          this.updateClient.employee_name = client.employee_name;
          this.updateClient.employee_age = client.employee_age;
          this.updateClient.employee_salary = client.employee_salary;
        }
      });
    }
  }
  // Can also be close ny esc ,backdrop
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //delete data
  deleteClient(id) {
    this.apiHttp.delete(id).subscribe((data) => {
      console.log(data);
      alert('deleted');
      this.clientDetailData = this.clientDetailData.filter(item => item.id !== id);


    });
  }
}

