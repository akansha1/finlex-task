import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AppHttpService {

  constructor(private httpClient: HttpClient) { }
  public get() {
    return this.httpClient.get(`http://dummy.restapiexample.com/api/v1/employees`);
  }
  public getDetail(id:number) {
    return this.httpClient.get(`http://dummy.restapiexample.com/api/v1/employee/`+id);
  }
  public delete(id:number){
    return this.httpClient.delete(`http://dummy.restapiexample.com/api/v1/delete/`+id);
  } 
  public post(data:any){
    return this.httpClient.post<any>(`http://dummy.restapiexample.com/api/v1/create`, data);
  }
}
